package com.example.realt.realtapp.view;

public interface ActivityCallback {

    void showProgressBar();

    void hideProgressBar();
}
