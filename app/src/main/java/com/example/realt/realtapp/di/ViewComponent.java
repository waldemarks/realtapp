package com.example.realt.realtapp.di;

import com.example.realt.realtapp.view.fragments.ApartmentsFragment;

import javax.inject.Singleton;

import dagger.Component;

@Singleton
@Component(modules = {ViewModule.class})
public interface ViewComponent {

    void inject(ApartmentsFragment fragment);

}
