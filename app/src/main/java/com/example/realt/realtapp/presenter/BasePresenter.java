package com.example.realt.realtapp.presenter;

import com.example.realt.realtapp.di.DaggerSubscriptionComponent;
import com.example.realt.realtapp.di.PresenterModule;
import com.example.realt.realtapp.di.SubscriptionComponent;

import javax.inject.Inject;

import rx.Subscription;
import rx.subscriptions.CompositeSubscription;

public abstract class BasePresenter implements Presenter {

    private SubscriptionComponent subscriptionComponent;

    @Inject
    protected CompositeSubscription compositeSubscription;

    public BasePresenter() {
        if (subscriptionComponent == null) {
            subscriptionComponent = DaggerSubscriptionComponent.builder()
                    .presenterModule(new PresenterModule())
                    .build();
        }
        subscriptionComponent.inject(this);
    }

    protected void addSubscription(Subscription subscription) {
        compositeSubscription.add(subscription);
    }

    @Override
    public void onStop() {
        compositeSubscription.clear();
    }

}
