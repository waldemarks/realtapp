package com.example.realt.realtapp.di;

import com.example.realt.realtapp.Const;
import com.example.realt.realtapp.model.networking.NetworkApi;
import com.example.realt.realtapp.model.networking.NetworkApiInterface;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;

@Module
public class NetworkModule {

    @Provides
    @Singleton
    NetworkApiInterface provideApiInterface() {
        return NetworkApi.getNetworkApi(Const.URL);
    }

}
