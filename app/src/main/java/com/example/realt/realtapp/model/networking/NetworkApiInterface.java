package com.example.realt.realtapp.model.networking;

import com.example.realt.realtapp.model.apartments.Apartments;

import retrofit2.http.GET;
import retrofit2.http.Headers;
import retrofit2.http.Query;
import rx.Observable;

public interface NetworkApiInterface {

    @Headers({
            "Accept: application/json",
            "Content-Type: application/json"
    })
    @GET("apartments?")
    Observable<Apartments> getApartments(@Query("page") int page);

}
