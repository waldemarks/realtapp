package com.example.realt.realtapp.di;

import com.example.realt.realtapp.presenter.ApartmentsPresenter;
import com.example.realt.realtapp.presenter.BasePresenter;

import javax.inject.Singleton;

import dagger.Component;

@Singleton
@Component(modules = {NetworkModule.class, PresenterModule.class})
public interface NetworkComponent {

    void inject(BasePresenter basePresenter);

    void inject(ApartmentsPresenter apartmentsPresenter);

}
