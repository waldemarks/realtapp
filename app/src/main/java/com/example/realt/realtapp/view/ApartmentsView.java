package com.example.realt.realtapp.view;

import com.example.realt.realtapp.model.apartments.Apartment;

import java.util.List;

public interface ApartmentsView {

    void showApartments(List<Apartment> apartmentsList);

    void showEmptyList();

    void showError(String error);

    void showLoading();

    void hideLoading();

}
