package com.example.realt.realtapp.view.fragments;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.realt.realtapp.R;
import com.example.realt.realtapp.di.DaggerViewComponent;
import com.example.realt.realtapp.di.ViewComponent;
import com.example.realt.realtapp.di.ViewModule;
import com.example.realt.realtapp.model.apartments.Apartment;
import com.example.realt.realtapp.model.apartments.Apartments;
import com.example.realt.realtapp.presenter.ApartmentsPresenter;
import com.example.realt.realtapp.view.ActivityCallback;
import com.example.realt.realtapp.view.ApartmentsView;
import com.example.realt.realtapp.view.adapters.ApartmentsRecyclerViewAdapter;
import com.example.realt.realtapp.view.widgets.EndlessRecyclerViewScrollListener;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import butterknife.Bind;
import butterknife.ButterKnife;

public class ApartmentsFragment extends Fragment implements ApartmentsView {

    private static final int FIRST_PAGE = 1;

    @Bind(R.id.recycler_view)
    RecyclerView recyclerView;

    @Inject
    protected ApartmentsPresenter presenter;

    private ViewComponent viewComponent;

    private ApartmentsRecyclerViewAdapter mAdapter;
    private ActivityCallback activityCallback;

    private List<Apartment> mApartments;

    public ApartmentsFragment() {
    }

    public static ApartmentsFragment newInstance() {

        Bundle args = new Bundle();

        ApartmentsFragment fragment = new ApartmentsFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);

        try {
            activityCallback = (ActivityCallback) context;
        } catch (ClassCastException e) {
            throw new ClassCastException(context.toString()
                    + " must implement activityCallback");
        }
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (viewComponent == null) {
            viewComponent = DaggerViewComponent.builder()
                    .viewModule(new ViewModule(this))
                    .build();
        }
        viewComponent.inject(this);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_main, container, false);
    }


    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        ButterKnife.bind(this, view);

        LinearLayoutManager llm = new LinearLayoutManager(getContext());
        recyclerView.setLayoutManager(llm);
        mAdapter = new ApartmentsRecyclerViewAdapter(getActivity(), new Apartments(), presenter);
        recyclerView.setAdapter(mAdapter);

        recyclerView.addOnScrollListener(new EndlessRecyclerViewScrollListener(llm) {
            @Override
            public void onLoadMore(int page, int totalItemsCount) {
                presenter.onSearch(page + 1);
            }
        });

        presenter.onViewCreated(savedInstanceState);

        if (savedInstanceState == null) {
            presenter.onSearch(FIRST_PAGE);
        }

    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        ButterKnife.unbind(this);
    }

    @Override
    public void showApartments(List<Apartment> apartmentsList) {
        if (apartmentsList != null && !apartmentsList.isEmpty()) {
            if (mApartments == null) {
                mApartments = new ArrayList<>();
            }
            mApartments.addAll(apartmentsList);
        }
        mAdapter.setApartmentsList(mApartments);
    }

    @Override
    public void showEmptyList() {
        Snackbar.make(recyclerView, getString(R.string.empty_list), Snackbar.LENGTH_LONG).show();
    }

    @Override
    public void showError(String error) {
        Snackbar.make(recyclerView, error, Snackbar.LENGTH_LONG).show();
    }

    @Override
    public void showLoading() {
        activityCallback.showProgressBar();
    }

    @Override
    public void hideLoading() {
        activityCallback.hideProgressBar();
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        presenter.onSaveInstanceState(outState, mApartments);
    }
}
