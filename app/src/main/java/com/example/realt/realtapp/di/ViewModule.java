package com.example.realt.realtapp.di;

import com.example.realt.realtapp.presenter.ApartmentsPresenter;
import com.example.realt.realtapp.view.ApartmentsView;

import dagger.Module;
import dagger.Provides;

@Module
public class ViewModule {

    private ApartmentsView view;

    public ViewModule(ApartmentsView view) {
        this.view = view;
    }

    @Provides
    ApartmentsPresenter provideRepoListPresenter() {
        return new ApartmentsPresenter(view);
    }

}
