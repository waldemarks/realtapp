package com.example.realt.realtapp.view.adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.realt.realtapp.R;
import com.example.realt.realtapp.model.apartments.Apartment;
import com.example.realt.realtapp.model.apartments.Apartments;
import com.example.realt.realtapp.presenter.ApartmentsPresenter;
import com.squareup.picasso.Picasso;

import java.util.List;

import butterknife.Bind;
import butterknife.ButterKnife;

public class ApartmentsRecyclerViewAdapter extends RecyclerView.Adapter<ApartmentsRecyclerViewAdapter.ViewHolder> {

    private static final String ROOM = "room";
    private static final String ROOM_1 = "1_room";
    private static final String ROOM_2 = "2_rooms";
    private static final String ROOM_3 = "3_rooms";
    private static final String ROOM_4 = "4_rooms";
    private static final String ROOM_5 = "5_rooms";
    private static final String ROOM_6 = "6_rooms";
    private static final String ROOM_7 = "7_rooms";

    private Context mContext;
    private List<Apartment> mValues;
    private ApartmentsPresenter mPresenter;

    public ApartmentsRecyclerViewAdapter(Context context, Apartments items, ApartmentsPresenter presenter) {
        mContext = context;
        mValues = items.getApartments();
        mPresenter = presenter;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.fragment_apartment_item, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, int position) {
        holder.item = mValues.get(position);
        holder.addressTv.setText(holder.item.getLocation().getAddress());
        String rentType = "";
        switch (holder.item.getRentType()) {
            case ROOM:
                rentType = mContext.getString(R.string.room);
                break;
            case ROOM_1:
                rentType = mContext.getString(R.string.room_1);
                break;
            case ROOM_2:
                rentType = mContext.getString(R.string.room_2);
                break;
            case ROOM_3:
                rentType = mContext.getString(R.string.room_3);
                break;
            case ROOM_4:
                rentType = mContext.getString(R.string.room_4);
                break;
            case ROOM_5:
                rentType = mContext.getString(R.string.room_5);
                break;
            case ROOM_6:
                rentType = mContext.getString(R.string.room_6);
                break;
            case ROOM_7:
                rentType = mContext.getString(R.string.room_7);
                break;
        }
        holder.rentTypeTv.setText(rentType);
        holder.bynPriceTv.setText(mContext.getString(R.string.plus_ruble, holder.item.getPrice().getConverted().getBYN().getAmount()));
        holder.usdPriceTv.setText(mContext.getString(R.string.plus_dollar, holder.item.getPrice().getConverted().getUSD().getAmount()));
        holder.byrPriceTv.setText(mContext.getString(R.string.plus_ruble, holder.item.getPrice().getConverted().getBYR().getAmount()));

        Picasso.with(mContext)
                .load(holder.item.getPhoto())
                .fit()
                .centerCrop()
                .placeholder(R.drawable.black_gradient_holder)
                .error(R.drawable.black_gradient_holder)
                .into(holder.backgroundIv);

        holder.view.setOnClickListener(v -> mPresenter.onApartmentItemClick(holder.item));
    }

    @Override
    public int getItemCount() {
        return mValues.size();
    }

    public void setApartmentsList(List<Apartment> apartments) {
        mValues = apartments;
        notifyDataSetChanged();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        @Bind(R.id.layout)
        View view;
        @Bind(R.id.backgroundIv)
        ImageView backgroundIv;
        @Bind(R.id.addressTv)
        TextView addressTv;
        @Bind(R.id.rentTypeTv)
        TextView rentTypeTv;
        @Bind(R.id.bynPriceTv)
        TextView bynPriceTv;
        @Bind(R.id.usdPriceTv)
        TextView usdPriceTv;
        @Bind(R.id.byrPriceTv)
        TextView byrPriceTv;
        public Apartment item;

        public ViewHolder(View view) {
            super(view);
            ButterKnife.bind(this, view);
        }

    }
}
