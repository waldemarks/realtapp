package com.example.realt.realtapp.presenter;

import android.os.Bundle;
import android.os.Parcelable;
import android.util.Log;

import com.example.realt.realtapp.di.DaggerNetworkComponent;
import com.example.realt.realtapp.di.NetworkComponent;
import com.example.realt.realtapp.di.NetworkModule;
import com.example.realt.realtapp.model.apartments.Apartment;
import com.example.realt.realtapp.model.apartments.Apartments;
import com.example.realt.realtapp.model.networking.NetworkApiInterface;
import com.example.realt.realtapp.view.ApartmentsView;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import rx.Observable;
import rx.Subscriber;
import rx.Subscription;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

public class ApartmentsPresenter extends BasePresenter {

    private static final String BUNDLE_APARTMENTS = "bundle_apartments";

    private List<Apartment> mApartmentsList;

    private ApartmentsView mView;

    private NetworkComponent networkComponent;

    @Inject
    NetworkApiInterface networkApiInterface;

    public ApartmentsPresenter(ApartmentsView view) {
        if (networkComponent == null) {
            networkComponent = DaggerNetworkComponent.builder()
                    .networkModule(new NetworkModule())
                    .build();
        }
        networkComponent.inject(this);
        mView = view;
    }

    public void onViewCreated(Bundle savedInstanceState) {
        if (savedInstanceState != null) {
            mApartmentsList = savedInstanceState.getParcelableArrayList(BUNDLE_APARTMENTS);
        }
        if (isListNotEmpty()) {
            mView.showApartments(mApartmentsList);
        }
    }

    public void onSearch(int page) {
        mView.showLoading();
        Observable<Apartments> observable = networkApiInterface.getApartments(page);
        Subscription subscription = observable
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Subscriber<Apartments>() {
                    @Override
                    public void onCompleted() {
                        mView.hideLoading();
                        Log.d("complete", "complete");
                    }

                    @Override
                    public void onError(Throwable e) {
                        mView.hideLoading();
                        mView.showError(e.getMessage());
                        Log.e("error", e.toString());
                    }

                    @Override
                    public void onNext(Apartments apartments) {
                        Log.d("response", "success");
                        if (apartments != null) {
                            mView.showApartments(apartments.getApartments());
                        } else {
                            mView.showEmptyList();
                        }
                    }
                });
        addSubscription(subscription);
    }

    private boolean isListNotEmpty() {
        return (mApartmentsList != null && !mApartmentsList.isEmpty());
    }

    public void onApartmentItemClick(Apartment apartment) {
        //TODO click start apartment fragment
    }

    public void onSaveInstanceState(Bundle outState, List<Apartment> apartments) {
        outState.putParcelableArrayList(BUNDLE_APARTMENTS, (ArrayList<? extends Parcelable>) apartments);
    }
}
