package com.example.realt.realtapp.di;

import com.example.realt.realtapp.presenter.BasePresenter;

import javax.inject.Singleton;

import dagger.Component;

@Singleton
@Component(modules = {PresenterModule.class})
public interface SubscriptionComponent {

    void inject(BasePresenter basePresenter);

}
